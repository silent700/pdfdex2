#!/bin/bash
# pdfdex2.sh
# A PDF file metadata extractor and index creator
# Possibly even cover pages, too
# Requires pdfinfo and pdfimages from the popper-utils package, imagemagick 
# Usage: pdfdex2 [top path to pdf files]
#
# V1.0 5/10/2020 silent700 at google's mail service

# Testing

# Global vars
baseurl=http://vtda.org/docs
tdir=$2
if [ "$tdir" = "" ]; then
 tdir=$PWD
fi

# Functions
report () {
 # establish vars
 col=`echo $1 | cut -d/ -f1`
 dname=`dirname "$1"`
 fname=`basename "$1"`
 csum=`md5sum "$1"|cut -d' ' -f1`
 url=$baseurl/"$1"
 dirurl=$baseurl/$dname
 author=`pdfinfo "$1" | grep Author: | tr -s ' ' | cut -d' ' -f2-`
 title=`pdfinfo "$1" | grep Title: | tr -s ' ' | cut -d' ' -f2-`
 pages=`pdfinfo "$1" | grep Pages: | tr -s ' ' | cut -d' ' -f2`
 size=`pdfinfo "$1" | grep "File size:" | tr -s ' ' | cut -d' ' -f3`
 size=$((size/1024))
 kwords=`pdfinfo "$1" | grep Keywords: | tr -s ' ' | cut -d' ' -f2-`

 # catch files with empty metadata fields for later processing
 if [ "$author" = "" ]; then 
  author="METADATA NOT FOUND"
 fi
 if [ "$title" = "" ]; then 
  title="METADATA NOT FOUND"
 fi
 if [ "$kwords" = "" ]; then 
  title="METADATA NOT FOUND"
 fi

 # print report
 echo Input path: $1
 echo Collection: $col
 echo Directory: $dname
 echo Filename: $fname
 echo Author: $author
 echo Title: $title
 echo Keywords: $kwords
 echo Pagecount: $pages
 echo Size: $size KB
 echo Directory URL: $dirurl
 echo URL: $url
 echo MD5: $csum
 echo
 echo ------------------
 echo
}

htmlreport () {
  echo "Haven't written the HTML processor yet"
}

csvreport () {
 # establish vars
 col=`echo $1 | cut -d/ -f1`
 dname=`dirname "$1"`
 fname=`basename "$1"`
 csum=`md5sum "$1"|cut -d' ' -f1`
 url=$baseurl/"$1"
 dirurl=$baseurl/$dname
 author=`pdfinfo "$1" | grep Author: | tr -s ' ' | tr -d , | cut -d' ' -f2-`
 title=`pdfinfo "$1" | grep Title: | tr -s ' ' | tr -d , | cut -d' ' -f2-`
 pages=`pdfinfo "$1" | grep Pages: | tr -s ' ' | cut -d' ' -f2`
 size=`pdfinfo "$1" | grep "File size:" | tr -s ' ' | cut -d' ' -f3`
 size=$((size/1024))
 kwords=`pdfinfo "$1" | grep Keywords: | tr -s ' ' | tr -d , | cut -d' ' -f2-`
 
 # catch files with empty metadata fields for later processing
 if [ "$author" = "" ]; then 
  author="NO MD"
 fi
 if [ "$title" = "" ]; then 
  title="NO MD"
 fi
 if [ "$kwords" = "" ]; then 
  title="NO MD"
 fi
 
 #output
 echo $1,$col,$dname,$fname,$author,$title,$kwords,$pages,$size,$dirurl,$url,$csum
}

mreport () {
 author=`pdfinfo "$1" | grep Author: | tr -s ' ' | cut -d' ' -f2`
 title=`pdfinfo "$1" | grep Title: | tr -s ' ' | cut -d' ' -f2`
 kwords=`pdfinfo "$1" | grep Keywords: | tr -s ' ' | cut -d' ' -f2`


if [ "$author" = "" ] || [ "$title" = "" ] || [ "$kwords" = "" ]; then
 mmsg="$1 is missing: "
 if [ "$author" = "" ]; then 
   mmsg="$mmsg author "
  fi
  if [ "$title" = "" ]; then 
   mmsg="$mmsg title "
  fi
  if [ "$kwords" = "" ]; then 
   mmsg="$mmsg keywords "
  fi 
 echo $mmsg
fi
}


# Main
case "$1" in
-csv)
  echo "path,collection,directory,filename,author,title,keywords,pagecount,size (KB),dir URL,URL,MD5"
  cd $tdir/..
  find `basename "$tdir"` -type f -iname '*.pdf' | while read file; do csvreport "$file"; done
;;
-html) 
  cd $tdir/..
  find `basename "$tdir"` -type f -iname '*.pdf' | while read file; do htmlreport "$file"; done
;;
-m)
  cd $tdir/..
  find `basename "$tdir"` -type f -iname '*.pdf' | while read file; do mreport "$file"; done
;;
-r)
  cd $tdir/..
  find `basename "$tdir"` -type f -iname '*.pdf' | while read file; do report "$file"; done
;;

*)
  echo "Usage  pdfdex2 -html <input path> HTML-formatted report"
  echo "       pdfdex2 -csv <input path> character-separated value report"
  echo "       pdfdex2 -r <input path> plain text report"
  echo "       pdfdex2 -m <input path> missing metadata report"
  echo "       pdfdex2 -h this help text"
;;
esac
